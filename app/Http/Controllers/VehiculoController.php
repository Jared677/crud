<?php

namespace App\Http\Controllers;

use App\Vehiculo;
use App\Models\historico;
use App\Usuario;
use Illuminate\Http\Request;

/**
 * Class VehiculoController
 * @package App\Http\Controllers
 */
class VehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehiculos = Vehiculo::paginate();

        return view('vehiculo.index', compact('vehiculos'))
            ->with('i', (request()->input('page', 1) - 1) * $vehiculos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehiculo = new Vehiculo();
        $historico = new Historico();
        $usuarios = Usuario::pluck('nombre','id');
        return view('vehiculo.create', compact('vehiculo','usuarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Vehiculo::$rules);

        $vehiculo = Vehiculo::create($request->all());

        $historico = new Historico();
        $historico->nombre_marca = $vehiculo->marca;
        $historico->nombre_modelo = $vehiculo->modelo;
        $historico->año = $vehiculo->año;
        $historico->precio = $vehiculo->precio;
        $historico->nombre_usuario = $vehiculo->usuario->nombre;
        $historico->save();

        return redirect()->route('vehiculo.index')
            ->with('success', 'Vehiculo created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehiculo = Vehiculo::find($id);

        return view('vehiculo.show', compact('vehiculo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehiculo = Vehiculo::find($id);
        $usuarios = Usuario::pluck('nombre','id');
        return view('vehiculo.edit', compact('vehiculo','usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Vehiculo $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehiculo $vehiculo)
    {
        request()->validate(Vehiculo::$rules);

        $vehiculo->update($request->all());

        $historico = new Historico();
        $historico->nombre_marca = $vehiculo->marca;
        $historico->nombre_modelo = $vehiculo->modelo;
        $historico->año = $vehiculo->año;
        $historico->precio = $vehiculo->precio;
        $historico->nombre_usuario = $vehiculo->usuario->nombre;
        $historico->save();

        return redirect()->route('vehiculo.index')
            ->with('success', 'Vehiculo actualizado exitosamente');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $vehiculo = Vehiculo::find($id)->delete();

        return redirect()->route('vehiculo.index')
            ->with('success', 'Vehiculo deleted successfully');
    }
}
