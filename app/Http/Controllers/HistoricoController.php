<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorehistoricoRequest;
use App\Http\Requests\UpdatehistoricoRequest;
use App\Models\historico;
use App\Usuario;
use App\Vehiculo;

class HistoricoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $historicos = Historico::paginate();
        $vehiculos = Vehiculo::paginate();
        $usuarios = Usuario::paginate();

        return view('historico.index', compact('historicos','vehiculos','usuarios'))
            ->with('i', (request()->input('page', 1) - 1) * $historicos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorehistoricoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorehistoricoRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function show(historico $historico)
    {
        //
        return view('historico.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function edit(historico $historico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatehistoricoRequest  $request
     * @param  \App\Models\historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatehistoricoRequest $request, historico $historico)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function destroy(historico $historico)
    {
        //
    }
}
