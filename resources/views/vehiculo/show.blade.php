@extends('layouts.app')

@section('template_title')
    {{ $vehiculo->name ?? 'Mostrar Vehiculo' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Informacion Vehiculo</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('vehiculo.index') }}"> Atras</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Usuario id:</strong>
                            {{ $vehiculo->usuario_id }}
                        </div>
                        <div class="form-group">
                            <strong>Marca:</strong>
                            {{ $vehiculo->marca }}
                        </div>
                        <div class="form-group">
                            <strong>Modelo:</strong>
                            {{ $vehiculo->modelo }}
                        </div>
                        <div class="form-group">
                            <strong>Año:</strong>
                            {{ $vehiculo->año }}
                        </div>
                        <div class="form-group">
                            <strong>Precio:</strong>
                            {{ $vehiculo->precio }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
