@extends('layouts.app')


@section('template_title')
    Historial
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Historial') }}
                            </span>

                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Usuario</th>
										<th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Año</th>
                                        <th>Precio</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($historicos as $historico)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $historico->nombre_usuario }}</td>
											<td>{{ $historico->nombre_marca }}</td>
                                            <td>{{ $historico->nombre_modelo }}</td>
                                            <td>{{ $historico->año }}</td>
                                            <td>{{ $historico->precio }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $historicos->links() !!}
            </div>
        </div>
    </div>
@endsection
