
# Tecnologías

Las tecnologías utilizadas fueron Laravel version 9.48 para el backend y NodeJS version 19.4 para el frontend. Además, la versión de php fue la 8.1.2.

# Comandos importantes

Es necesario correr primero el servidor con

```
php artisan serve
```

Además, para que funcione correctamente el front, es necesario el comando 

```
npm run dev
```

# Funcionalidades implementadas

En general es una pagina web que incluye las cuatro operaciones básicas que componen un CRUD (Create, read, update and delete), sin embargo, dado los requerimientos se considera no completada la accion de borrar, ya que no fue realizada con **soft delete** entregado por laravel. Por otro lado, las demás funciones son realizadas con normalidad, incluyendo un histórico de los dueños de vehiculos. Se incluyo un test unitario para la función de store perteneciente al controlador de usuario.

# Breve vistazo a la web

## Login

Para poder iniciar es necesario crearse una cuenta, basta con escoger cualquier correo de prueba con cualquier contraseña. Cabe destacar que de otra forma no es posible acceder al contenido de la página web.

<center> 
    <img src="imagenes/login.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.1 - Log in de la página web.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/registro.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.2 - Registro de la página web.</b>
        </figcaption>
</center>


## Vehiculos

Tal como se mencionó, es posible agregar, modificar y ver todos los vehículos disponibles.



<center> 
    <img src="imagenes/vehiculo_show.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.3 - Vista general de la lista de vehículos.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/vehiculo_edit.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.4 - Seleccionando el usuario quien sera dueño.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/vehiculo_edit2.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.5 - Llenando los demás parametros.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/vehiculo_details.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.6 - Viendo los detalles de cada vehículo.</b>
        </figcaption>
</center>

## Usuarios

Tal como se mencionó, es posible agregar, modificar y ver todos los vehículos disponibles.

<center> 
    <img src="imagenes/usuario_show.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.7 - Vista general de la lista de usuarios.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/usuario_edit.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.8 - Creando un nuevo usuario.</b>
        </figcaption>
</center>

<center> 
    <img src="imagenes/usuario_details.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.9 - Viendo los detalles de un usuario.</b>
        </figcaption>
</center>


##  Historial

Aquí se muestra un histórico de todos los vehiculos y sus respectivos dueños.

<center> 
    <img src="imagenes/historial.png" style="width:100%">
        <figcaption align = "center">
            <b>Fig.10 - Tabla con el historial de los usarios y vehículos.</b>
        </figcaption>
</center>


