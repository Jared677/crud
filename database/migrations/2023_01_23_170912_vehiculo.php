<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('vehiculos', function (Blueprint $table) {

            
            $table->bigIncrements('id');
            $table->bigInteger('usuario_id')->unsigned();
            $table->string("marca");
            $table->string("modelo");
            $table->integer("año");
            $table->integer("precio");
            $table->timestamps();
            $table->foreign("usuario_id")->references("id")->on("usuarios");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
