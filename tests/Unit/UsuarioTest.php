<?php

namespace Tests\Unit;

use Tests\TestCase;

class UsuarioTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->call('POST','usuario',[ //$rules
            'nombre' => 'placeholdernombre',
            'apellido' => 'placeholderapellido',
            'correo' => 'placeholder@correo.com',
        ]);

        $response->assertStatus($response->status(), 302);
    }
}
